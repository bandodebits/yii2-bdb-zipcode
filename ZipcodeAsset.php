<?php

namespace bdb\zipcode;

use yii\web\AssetBundle;

class ZipcodeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bandodebits/yii2-bdb-zipcode/assets';

    public $js = [
        'js/jquery.cep.js',
    ];
} 